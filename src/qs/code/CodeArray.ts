module qs {
    export class CodeArray extends CodeObject {
        public readonly elements: CodeObject[] = [];
    }
}